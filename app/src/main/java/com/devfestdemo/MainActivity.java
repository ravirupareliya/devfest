package com.devfestdemo;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

public class MainActivity extends AppCompatActivity {
    public static final String TAG = "devfest";
    private EditText edt;
    private ProgressBar pb;
    DatabaseReference registeredReference, attendeeReference;
    private Context context;
    private Map<String, Object> map;
    private LayoutInflater inflater;
    private LinearLayout llData;
    private Button btnConfirm;
    private User user;
    private long childrenCount;
    private String registerationNumber;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        context = this;
        inflater = LayoutInflater.from(this);
        llData = (LinearLayout) findViewById(R.id.linearContent);
        btnConfirm = (Button) findViewById(R.id.btnConfirm);
        edt = (EditText) findViewById(R.id.edt);
        pb = (ProgressBar) findViewById(R.id.pb);

        FirebaseDatabase database = FirebaseDatabase.getInstance();
        registeredReference = database.getReference("registered");
        attendeeReference = database.getReference("attendee");

        edt.addTextChangedListener(new TextWatcher() {
            private Timer timer = new Timer();
            private final long DELAY = 1000;

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                btnConfirm.setVisibility(View.GONE);
                llData.setVisibility(View.GONE);
                llData.removeAllViews();
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                timer.cancel();
                timer = new Timer();
                if (editable.toString().length() > 0)
                    timer.schedule(
                            new TimerTask() {
                                @Override
                                public void run() {
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            timer.cancel();
                                            onSearchClick();
                                        }
                                    });

                                }
                            },
                            DELAY
                    );
            }
        });

    }

    public void onSearchClick() {
        pb.setVisibility(View.VISIBLE);

        registerationNumber = edt.getText().toString().trim();
        if (registerationNumber.length() < 3) {
            registerationNumber = String.format("%03d", Integer.parseInt(registerationNumber));
        }

        Query query = registeredReference.orderByChild("Serial Number").equalTo("GDG-DevFestAhm-2017-" + registerationNumber);
        query.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Iterable<DataSnapshot> children = dataSnapshot.getChildren();
                User userSelected = null;
                for (DataSnapshot child : children) {
                    userSelected = child.getValue(User.class);
                    break;
                }

                pb.setVisibility(View.GONE);

                if (userSelected != null) {
                    user = userSelected;
                    fillDetail(userSelected);
                    btnConfirm.setVisibility(View.VISIBLE);
                    llData.setVisibility(View.VISIBLE);
                } else
                    Toast.makeText(MainActivity.this, "No record found.", Toast.LENGTH_SHORT).show();

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e(TAG, "onCancelled() called with: databaseError = [" + databaseError + "]");
                pb.setVisibility(View.GONE);

            }
        });
    }

    private void fillDetail(User user) {
        llData.removeAllViews();
        ObjectMapper oMapper = new ObjectMapper();
        map = oMapper.convertValue(user, Map.class);
        Log.e("MainActivity", "fillDetail: " + map);
        for (Map.Entry<String, Object> entry : map.entrySet()) {
            String key = entry.getKey();
            String value = String.valueOf(entry.getValue());

            View v = inflater.inflate(R.layout.item_user_data, null, false);
            TextView txtKey = (TextView) v.findViewById(R.id.txtKey);
            TextView txtValue = (TextView) v.findViewById(R.id.txtValue);
            txtKey.setText(key.substring(0, 1).toUpperCase() + key.substring(1));
            txtValue.setText(value.substring(0, 1).toUpperCase() + value.substring(1));

            llData.addView(v);
        }
    }

    public void onConfirmClick(View v) {
        attendeeReference.addValueEventListener(valueEventListener);
    }

    ChildEventListener childEventListener = new ChildEventListener() {
        @Override
        public void onChildAdded(DataSnapshot dataSnapshot, String s) {
            Toast.makeText(context, "Present sir :)", Toast.LENGTH_SHORT).show();
            btnConfirm.setVisibility(View.VISIBLE);
            llData.setVisibility(View.VISIBLE);
            edt.setText("");
        }

        @Override
        public void onChildChanged(DataSnapshot dataSnapshot, String s) {

        }

        @Override
        public void onChildRemoved(DataSnapshot dataSnapshot) {

        }

        @Override
        public void onChildMoved(DataSnapshot dataSnapshot, String s) {

        }

        @Override
        public void onCancelled(DatabaseError databaseError) {

        }
    };

    ValueEventListener valueEventListener = new ValueEventListener() {

        @Override
        public void onDataChange(DataSnapshot dataSnapshot) {
            childrenCount = dataSnapshot.getChildrenCount();
            attendeeReference.removeEventListener(valueEventListener);
            Iterable<DataSnapshot> children = dataSnapshot.getChildren();
            boolean childAvailable = false;
            for (DataSnapshot child : children) {
                User dummyUser = child.getValue(User.class);
                if (user.serialNumber.equalsIgnoreCase(dummyUser.serialNumber)) {
                    childAvailable = true;
                    break;
                }
            }
            if (!childAvailable) {
                attendeeReference.child(registerationNumber).setValue(user);
                attendeeReference.addChildEventListener(childEventListener);
            } else {
                Toast.makeText(context, "Already attended.", Toast.LENGTH_SHORT).show();
                btnConfirm.setVisibility(View.VISIBLE);
                llData.setVisibility(View.VISIBLE);
                edt.setText("");
            }
        }

        @Override
        public void onCancelled(DatabaseError databaseError) {

        }
    };
}