package com.devfestdemo;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.firebase.database.PropertyName;

/**
 * Created by AND004 on 10/30/2017.
 */

public class User implements Parcelable {
    @PropertyName("Contact")
    public String contact;

    @PropertyName("Address")
    public String address;

    @PropertyName("Description")
    public String description;

    @PropertyName("Email")
    public String email;

    @PropertyName("Name")
    public String name;

    @PropertyName("Serial Number")
    public String serialNumber;

    @PropertyName("Session")
    public String session;

    public User() {
    }

    public User(String contact, String address, String description, String email, String name, String serialNumber, String session) {
        this.contact = contact;
        this.address = address;
        this.description = description;
        this.email = email;
        this.name = name;
        this.serialNumber = serialNumber;
        this.session = session;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(contact);
        parcel.writeString(address);
        parcel.writeString(description);
        parcel.writeString(email);
        parcel.writeString(name);
        parcel.writeString(serialNumber);
        parcel.writeString(session);
    }

    // this is used to regenerate your object. All Parcelables must have a CREATOR that implements these two methods
    public static final Parcelable.Creator<User> CREATOR = new Parcelable.Creator<User>() {
        public User createFromParcel(Parcel in) {
            return new User(in);
        }

        public User[] newArray(int size) {
            return new User[size];
        }
    };

    // example constructor that takes a Parcel and gives you an object populated with it's values
    public User(Parcel in) {
        contact = in.readString();
        address = in.readString();
        description = in.readString();
        email = in.readString();
        name = in.readString();
        serialNumber = in.readString();
        session = in.readString();

    }
}
